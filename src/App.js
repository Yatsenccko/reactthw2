import React from 'react';
import Button from './components/button';
import Modal from './components/modal';
import './App.css';
import ProductList from './components/productList/productList';
import Header from './components/header';




class App extends React.Component {
  constructor() {
    super()
    this.state = {
      productList: [],
      currentId: null,
      countFav: JSON.parse(localStorage.getItem("fav"))?.length || 0,
      countCart: JSON.parse(localStorage.getItem("cart")) || [] ,
      modalFirst: {
        isOpen: false,
        id: "modalFirst",
        title: "titleDelete",
        text: "Do you want delete?",
        isCloseBtn: true,
        onClose: (id) => this.closeModal(id),
        
      },
      modalAddToCart: {
        isOpen: false,
        id: "modalAddToCart",
        title: "Add to cart",
        text: "Do you want add to cart?",
        isCloseBtn: true,
        onClose: (id) => this.closeModal(id),
        actions: {
          btnSubmit: <Button text="Submit" styles="yellow" onClick={() => { this.closeModal("modalAddToCart") }} />,
          btnCancel: <Button text="Cancel" styles="purple" onClick={() => { this.closeModal("modalAddToCart") }} />
        }
      },

    }
  }

  componentDidMount() {
    (async () => {
      const res = await fetch("./db.json")
      const products = await res.json()
      this.createStorage()
      const storage = this.createStorage()
      const checkProducts = this.checkInFavAndInCart(products.products, storage)
      this.setState((prev) => {
        const newState = { ...prev }
        newState.productList = checkProducts
        return newState
      })
    })()
  }

  createStorage() {
    const cart = JSON.parse(localStorage.getItem("cart"))
    const fav = JSON.parse(localStorage.getItem("fav"))
    if (!cart || !fav) {
      localStorage.setItem("cart", JSON.stringify([]))
      localStorage.setItem("fav", JSON.stringify([]))
    }
    return {
      fav, cart
    }
  }

  checkInFavAndInCart(products, { fav, cart }) {
    const updateInCart = products.map(item => {
      if (cart.includes(item.id)) {
        item.cart = true
        return item
      }
      if (fav.includes(item.id)) {
        item.fav = true
        return item
      }
      return item
    })
    return updateInCart

  }

  openModal(id, productId) { 
    this.setState(current => {
      const newState = { ...current }
      newState[id].isOpen = true
          newState.currentId = productId 
      return newState;
    })
  }

  closeModal(id) {
    this.setState(current => {
      const newState = { ...current }
      newState[id].isOpen = false
      newState.currentId = null
      return newState;
    })
  }

  addProductToCart(currentId, id) {
    const storage = JSON.parse(localStorage.getItem("cart"))
    const finded = storage.find(item => item.id === currentId)
   
    if (!finded) {
      const newStorage = [...storage, {id: currentId, count: 1 }]
      localStorage.setItem("cart", JSON.stringify(newStorage))
      this.setState(current => {
        const newState = { ...current }
        newState.productList = newState.productList.map(item => {
          if (item.id === currentId) {
            item.cart = true
            return item
          }
          return item
        })
        newState.count = newStorage
        newState[id].isOpen = false
        newState.currentId = null
        return newState
      })
      return
    }
    if (finded) {
      let count = finded.count + 1
      console.log(count);
      let findedIndex = storage.findIndex(item => item.id === currentId)
      const newStorage =  storage.toSpliced(findedIndex, 1, {
        id: currentId, count
      })
      localStorage.setItem("cart", JSON.stringify(newStorage))
      this.setState(current => {
        const newState = { ...current }
        newState.count = newStorage
        newState[id].isOpen = false
        newState.currentId = null

        return newState
      })
    }
  }

  addProductToFav(id) {
    const storage = JSON.parse(localStorage.getItem("fav"))
    const finded = storage.find(item => item === id)
    if (!finded) {
      const newStorage = [...storage, id]
      localStorage.setItem("fav", JSON.stringify(newStorage))
      this.setState(current => {
        const newState = { ...current }
        newState.productList = newState.productList.map(item => {
          if (item.id === id) {
            item.fav = true
            return item
          }
          return item
        })
        newState.countFav += 1
        return newState
      })
      return
    }
    if (finded) {
      const newStorage = storage.filter(item => item !== id)
      localStorage.setItem("fav", JSON.stringify(newStorage))
      this.setState(current => {
        const newState = { ...current }
        newState.productList = newState.productList.map(item => {
          if (item.id === id) {
            item.fav = false
            return item
          }
          return item
        })
        newState.countFav -= 1

        return newState
      })
      return

    }
  }
  render() {
    const {
      productList,
      countCart,
      countFav,
      currentId
    } = this.state
    return (
      <div className="App">
        <Header fav = {countFav} cart = {countCart} />
        {/* <AiOutlineShoppingCart onClick={() => { this.openModal("modalFirst") }} ></AiOutlineShoppingCart> */}
        {/* <p>0</p>
        <AiOutlineHeart></AiOutlineHeart> */}
        <ProductList products={productList} addProductToFav={this.addProductToFav.bind(this)} modalOpen={this.openModal.bind(this)} />

        {this.state.modalFirst.isOpen && <Modal {...this.state.modalFirst} />}
        {this.state.modalAddToCart.isOpen && <Modal {...this.state.modalAddToCart} currentId = {currentId}  addProductToCart = {this.addProductToCart.bind(this)} />}



      </div>
    );
  }
}
export default App;
