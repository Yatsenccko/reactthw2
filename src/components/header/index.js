import React from "react";
import { AiOutlineShoppingCart,AiOutlineHeart } from 'react-icons/ai';
import { StyledHeader } from "./styled";
class Header extends React.Component{
    render(){
        console.log(this.props);
        const{
            fav,
            cart,
        } = this.props
        const countCart = cart.reduce((acc,cur)=>{
            acc += cur.count
            return acc
        }, 0) 
        return(
            
            <StyledHeader>
                <p><AiOutlineHeart/>{fav}</p>
                <p><AiOutlineShoppingCart/>{countCart}</p>
                </StyledHeader>
        )
    }
}
export default Header