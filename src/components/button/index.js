import React from "react"
import {StyledBtn} from "./styled"
class Button extends React.Component {
    render() {
        const {
            text, onClick, styles, id, productId
        } = this.props
        return (
            <StyledBtn onClick={()=> onClick(id, productId)} $bgStyle = {styles} >{text}</StyledBtn>
        )
    }
}
export default Button