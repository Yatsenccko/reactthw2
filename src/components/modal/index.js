import React from "react";
import {StyledModal, ModalContent, StyledClose} from "./styled"
import PropTypes from "prop-types";
import Button from "../button"
class Modal extends React.Component {
    render() {
        const {title, onClose, id, text, isCloseBtn, actions, addProductToCart, currentId } = this.props
        console.log(actions);
        return (
            <StyledModal onClick = {()=>{onClose(id)}} >
                <ModalContent onClick={e => e.stopPropagation()} >
                    {isCloseBtn && <StyledClose  onClick = {()=>{onClose(id)}} >&times;</StyledClose>}
                    <h2>{title}</h2>
                    <p>{text}</p>
                    <Button text="Submit" styles="yellow" onClick={() => { addProductToCart(currentId, id)}} />,
                    {/* <Button text="Cancel" styles="purple" onClick={() => { closeModal("modalFirst") }} /> */}
                </ModalContent>
            </StyledModal>
        )
    }
}
Modal.propTypes = {
    title: PropTypes.string,
    children: PropTypes.string,
    id: PropTypes.string,
    onClose: PropTypes.func,
    addProductToCart: PropTypes.func,
    isCloseBtn: PropTypes.bool,
    text: PropTypes.string,
    action: PropTypes.string,
}
export default Modal