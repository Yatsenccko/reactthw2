import React, { useLayoutEffect } from "react";
import Product from  "./product"

class ProductList extends React.Component {
    render() {
        const {
            products,
            addProductToFav,
            modalOpen
        } = this.props
        return(
        <ul>
            {products.map(product => (
                <Product key = {product.id} {...product} addProductToFav = {addProductToFav} modalOpen = {modalOpen}/>
            ))}
        </ul>
        )
    }
}
export default ProductList