import React from "react";
import { StyledImg, StyledCard, StyledButton } from "./styled"
import { AiFillHeart, AiOutlineHeart, } from 'react-icons/ai';
import Button from "../button";
class Product extends React.Component {
    render() {
        const {
            title,
            imgSrc,
            price,
            article,
            color,
            fav,
            id,
            addProductToFav,
            modalOpen
        } = this.props
        
        return (
           
            <StyledCard>
                <p>{fav?<AiFillHeart onClick = {()=> addProductToFav(id)}/>: <AiOutlineHeart onClick = {()=> addProductToFav(id)}/>}</p>
               
                <h3>{title}</h3>
                <StyledImg src={imgSrc} />
                <p>{price}</p>
                <p>{article}</p>
                <p>{color}</p>
                <Button text="Add to cart" styles="blue" id="modalAddToCart" productId = {id} onClick = {modalOpen}/>
            </StyledCard>
               
        )
    }
}
export default Product